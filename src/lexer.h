/*
 * tokenanalyzer.h
 *
 *  Created on: Sep 21, 2016
 *      Author: drewctate
 */

#ifndef TOKENANALYZER_H_
#define TOKENANALYZER_H_

#include <list>
#include <sstream>
#include "token.h"

class lexer {
public:
	lexer(std::list<char>&);
	virtual ~lexer();
	bool analyze();
	std::list<token>* getTokens();

private:
	// vars
	std::list<char> toParse;
	std::list<char>::iterator it;
	int fileLength;
	int lineNum;
	int beginLineNum;
	int offset;
	std::stringstream sstream;
	std::list<token> tokens;

	// functions
	bool genNextTok();
	bool isReservedWd();
	bool isOther();
	bool isId();
	bool isString();
	bool isComment();
	bool isMultiLineComment();
	bool undefined();
	bool isEOF();

	bool finalChar(char);
	void rewind();

	// isSchemes
	bool isSchemes();
	bool sch_c();
	bool sch_h();
	bool sch_e();
	bool sch_m();
	bool sch_e2();

	// isFacts
	bool isFacts();
	bool fac_a();
	bool fac_c();
	bool fac_t();

	// isRules
	bool isRules();
	bool rul_u();
	bool rul_l();
	bool rul_e();

	// isQueries
	bool isQueries();
	bool que_u();
	bool que_e();
	bool que_r();
	bool que_i();
	bool que_e2();
};

#endif /* TOKENANALYZER_H_ */
