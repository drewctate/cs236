/*
 * Rule.h
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#ifndef RULE_H_
#define RULE_H_

#include <list>
#include <sstream>
#include "HeadPredicate.h"
#include "Predicate.h"

class Rule {
public:
	Rule();
	virtual ~Rule();

	void setHeadPredicate(HeadPredicate);
	HeadPredicate getHeadPredicate();
	std::list<Predicate>* getPredicates();
	void addPredicate(Predicate);
	void addPredicates(std::list<Predicate>&);

	std::string toString();

private:
	HeadPredicate headPredicate;
	std::list<Predicate> predicateList;
};

#endif /* RULE_H_ */
