/*
 * Rule.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#include <iostream>
#include "Rule.h"

Rule::Rule() :
predicateList(std::list<Predicate>()) {}

Rule::~Rule() {}

HeadPredicate Rule::getHeadPredicate() {
	return headPredicate;
}

void Rule::setHeadPredicate(HeadPredicate hp) {
	headPredicate = hp;
}

std::list<Predicate>* Rule::getPredicates() {
	return &(predicateList);
}

void Rule::addPredicate(Predicate p) {
	predicateList.push_back(p);
}

void Rule::addPredicates(std::list<Predicate>& lst) {
	for (std::list<Predicate>::iterator it = lst.begin(); it != lst.end(); it++) {
		addPredicate(*it);
	}
}

std::string Rule::toString() {
	std::stringstream stream;
	stream << headPredicate.toString() << " :- ";
	int count = 0;
	int predCount = predicateList.size();
	for (std::list<Predicate>::iterator it = predicateList.begin(); it != predicateList.end(); it++) {
		stream << it->toString();
		if (count < predCount - 1) {
			stream << ',';
		}
		count++;
	}
	stream << '.';
	return stream.str();
}
