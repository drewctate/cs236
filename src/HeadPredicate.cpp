/*
 * HeadPredicate.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: drewctate
 */

#include "HeadPredicate.h"

HeadPredicate::HeadPredicate() :
	name(token("ID", "uninitialized", 0)), idList(new std::list<token>()), idCount(0) {}

HeadPredicate::~HeadPredicate() {}

token HeadPredicate::getName() {
	return name;
}

void HeadPredicate::setName(token t) {
	name = t;
}

std::list<token>* HeadPredicate::getIds() {
	return idList;
}

void HeadPredicate::addId(token t) {
	idList->push_back(t);
	idCount++;
}

void HeadPredicate::addIds(std::list<token>& lst) {
	for (std::list<token>::iterator it = lst.begin(); it != lst.end(); it++) {
		addId(*it);
	}
}

std::string HeadPredicate::toString() {
	std::stringstream stream;
	stream << name.getValue() << "(";
	int count = 0;
	for (std::list<token>::iterator it = idList->begin(); it != idList->end(); it++) {
		stream << it->getValue();
		if (count < idCount - 1) {
			stream << ',';
		}
		count++;
	}
	stream << ")";
	return stream.str();
}

