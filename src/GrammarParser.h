/*
 * GrammerParser.h
 *
 *  Created on: Oct 2, 2016
 *      Author: drewctate
 */

#ifndef GRAMMERPARSER_H_
#define GRAMMERPARSER_H_

#include "token.h"
#include "DatalogProgram.h"
#include <list>
#include <iostream>
#include <sstream>

class GrammarParser {
public:
	enum grammar_t {SCHEMES, COLON, SCHEME, SCHEMELIST, FACTS, FACTLIST,
		RULES, RULELIST, QUERIES, QUERY, QUERYLIST, END};
	GrammarParser(std::list<token>&);
	virtual ~GrammarParser();
	DatalogProgram* getProgram();
	void parse();
	bool hadSuccess();

private:
	void match(grammar_t);
	void printTokens();
	void removeComments();

	void matchColon();
	void matchSchemes();
	void matchScheme();
	std::list<token>* matchIdList();
	void matchSchemeList();

	void matchFacts();
	void matchFact();
	std::list<token>* matchStringList();
	void matchFactList();

	void matchRules();
	void matchRuleList();
	void matchRule();
	HeadPredicate matchHeadPredicate();
	Predicate matchPredicate();
	void matchPredicate(Predicate&);
	std::list<Predicate>* matchPredicateList();
	void matchParameter(Predicate&);
	Parameter* matchParameter();
	void matchParameterList(Predicate&);
	Expression* matchExpression();
	void matchExpressionRec(Expression&);
	void matchOperator(Predicate&);
	char matchOperator();

	void matchQueries();
	void matchQuery();
	void matchQueryList();

	void matchEnd();
	std::list<token> tokens;
	std::list<token>::iterator it;
	DatalogProgram* dp;
	bool success;
};

#endif /* GRAMMERPARSER_H_ */
