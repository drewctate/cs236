/*
 * Database.h
 *
 *  Created on: Nov 1, 2016
 *      Author: drewctate
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include "Relation.h"
#include "DatalogProgram.h"
#include <sstream>

class Database {
public:
	Database(DatalogProgram&);
	virtual ~Database();
	std::string toString();
	Relation runQuery(Predicate);
	std::string runQueries();
	void evalRules();

private:
	std::string formatQueryResults(Predicate, Relation);
	void buildRelations();
	void populateRelations();
	Relation* findRelation(Predicate);
	DatalogProgram dp;
	std::list<Relation> relations;
	int rulePasses;
};

#endif /* DATABASE_H_ */
