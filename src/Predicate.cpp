/*
 * Predicate.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: drewctate
 */

#include <iostream>
#include "Predicate.h"

Predicate::Predicate() :
name(token("ID", "uninitialized", 0)), params(new std::list<Parameter*>) {}

Predicate::~Predicate() {}

void Predicate::setName(token t) {
	name = t;
}

token Predicate::getName() {
	return name;
}

std::list<Parameter*>* Predicate::getParams() {
	return params;
}

void Predicate::addParam(Parameter* p) {
	params->push_back(p);
}

void Predicate::addParams(std::list<Parameter*> lst) {
	for (std::list<Parameter*>::iterator it = lst.begin(); it != lst.end(); it++) {
		addParam(*it);
	}
}

std::string Predicate::toString() {
	std::stringstream stream;
	stream << name.getValue() << "(";
	int count = 0;
	int paramCount = params->size();
	for (std::list<Parameter*>::iterator it = params->begin(); it != params->end(); it++) {
		stream << (*it)->toString();
		if (count < paramCount - 1) {
			stream << ',';
		}
		count++;
	}
	stream << ")";
	return stream.str();
}
