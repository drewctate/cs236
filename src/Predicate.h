/*
 * Predicate.h
 *
 *  Created on: Oct 6, 2016
 *      Author: drewctate
 */

#ifndef PREDICATE_H_
#define PREDICATE_H_

#include <list>
#include "Parameter.h"
#include "token.h"

class Predicate {
public:
	Predicate();
	virtual ~Predicate();

	void setName(token);
	token getName();

	std::list<Parameter*>* getParams();
	void addParam(Parameter*);
	void addParams(std::list<Parameter*>);
	std::string toString();

private:
	token name;
	std::list<Parameter*>* params;
};

#endif /* PREDICATE_H_ */
