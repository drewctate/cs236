/*
 * Parameter.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: drewctate
 */

#include "Parameter.h"

Parameter::Parameter(std::string d) : data(d) {}

Parameter::~Parameter() {}

std::string Parameter::getData() {
	return data;
}

void Parameter::setData(std::string d) {
	data = d;
}

std::string Parameter::toString() {
	return data;
}

