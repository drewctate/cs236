/*
 * Node.cpp
 *
 *  Created on: Dec 5, 2016
 *      Author: drewctate
 */

#include "Node.h"

Node::Node(int val_in) : val(val_in) {
	// TODO Auto-generated constructor stub

}

Node::~Node() {
	// TODO Auto-generated destructor stub
}

int Node::getVal() {
	return val;
}

void Node::setTraversalNum(int num) {
	postTraversalNumber = num;
}

int Node::getTraversalNum() {
	return postTraversalNumber;
}

void Node::addChild(Node* node) {
	children.push_back(node);
}

std::vector<Node*>* Node::getChildren() {
	return &children;
}

void Node::removeChild(int val) {
	for (int i = 0; i < children.size(); i++) {
		if (children[i]->getVal() == val) {
			children.erase(children.begin()+i);
		}
	}
}

void Node::clearChildren() {
	children.clear();
}

std::string Node::toString() {
	std::stringstream stream;
	stream << 'R' << val;
	return stream.str();
}
