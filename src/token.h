/*
 * token.h
 *
 *  Created on: Sep 22, 2016
 *      Author: drewctate
 */

#ifndef TOKEN_H_
#define TOKEN_H_

#include <string>
#include <sstream>

class token {
public:
	token(std::string, std::string, int);
	virtual ~token();
	std::string getType();
	std::string getValue();
	std::string toString();
	int getLineNum();

private:
	int lineNum;
	std::string value;
	std::string type;
};

#endif /* TOKEN_H_ */
