/*
 * lexanalyze.cpp
 *
 *  Created on: Sep 13, 2016
 *      Author: drewctate
 */

#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <map>
#include "lexer.h"
#include "token.h"
#include "GrammarParser.h"
#include "Database.h"
#include "Node.h"
#include "Graph.h"

using namespace std;

int main (int argc, char* argv[]) {
//        list<char> lst;
//        std::ifstream in;
//        in.open("test1.txt");
//        if (in.is_open()) {
//                char c;
//                while (in.get(c)) {
//                        lst.push_back(c);
//                }
//        }
//        lst.push_back('_');
//        in.close();
//        lexer* t = new lexer(lst);
//        t->analyze();
//        list<token> tokens = *(t->getTokens());
//        GrammarParser* p = new GrammarParser(tokens);
//        p->parse();
//
//        if (!p->hadSuccess())
//                return 0;
//
//        Database* db = new Database(*(p->getProgram()));
//        db->evalRules();
////        cout << db->toString();
//        cout << db->runQueries();
//        return 0;
	/*
	 * START TEST 1
	 *
	 *
	 *
	 *
	 *
	 */

	Node* R0 = new Node(0);
	Node* R1 = new Node(1);
	Node* R2 = new Node(2);
	Node* R3 = new Node(3);
	Node* R4 = new Node(4);
	Node* R5 = new Node(5);
	Node* R6 = new Node(6);

	R3->addChild(R4);
	R3->addChild(R5);
	R3->addChild(R6);
	R6->addChild(R0);

	R2->addChild(R3);

	R1->addChild(R2);
	R1->addChild(R5);

	R0->addChild(R1);

	Graph graph = Graph();
	graph.addNode(R0);
	graph.addNode(R1);
	graph.addNode(R2);
	graph.addNode(R3);
	graph.addNode(R4);
	graph.addNode(R5);
	graph.addNode(R6);

	cout << "ORIGINAL GRAPH\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";
	cout << graph.toString();
	cout << "\n------------------------------------------------------------------------------------------------------------\n";

	/*
	 * END TEST 1
	 *
	 *
	 *
	 *
	 *
	 */

	cout << "BEGIN TEST 1: Post Order Traversal Numbers\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";
	cout << graph.postOrder(6);
	cout << "\n------------------------------------------------------------------------------------------------------------\n";


	/*
	 * START TEST 2
	 *
	 *
	 *
	 *
	 *
	 */

	cout << "BEGIN TEST 2: Find node corresponding to rule 5 (depth-first search)\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";
	cout << (graph.getNodeWVal(5))->toString();
	cout << "\n------------------------------------------------------------------------------------------------------------\n";


	/*
	 * END TEST 2
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 3
	 *
	 *
	 *
	 *
	 *
	 */

	cout << "BEGIN TEST 3: Report nodes reachable from R2\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";
	cout << graph.reachableFrom(2);
	cout << "\n------------------------------------------------------------------------------------------------------------\n";

	/*
	 * END TEST 3
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 4
	 *
	 *
	 *
	 *
	 *
	 */

	cout << "BEGIN TEST 4: Reverse edges\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";
	cout << "BEFORE:\n";
	cout << graph.toString();

	graph.reverse();
	cout << "\nAFTER:\n";
	cout << graph.toString();
	cout << "------------------------------------------------------------------------------------------------------------\n";

	/*
	 * END TEST 4
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 5
	 *
	 *
	 *
	 *
	 *
	 */

	cout << "BEGIN TEST 5: Reverse edges back (should produce original graph)\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";
	cout << "BEFORE:\n";
	cout << graph.toString();

	graph.reverse();
	cout << "\nAFTER:\n";
	cout << graph.toString();
	cout << "------------------------------------------------------------------------------------------------------------\n";


	/*
	 * END TEST 5
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 6
	 *
	 *
	 *
	 *
	 *
	 */


	cout << "NEW GRAPH IN PREPARATION FOR SCC COMPUTATION: \n";

	R0 = new Node(0);
	R1 = new Node(1);
	R2 = new Node(2);
	R3 = new Node(3);
	R4 = new Node(4);


	R0->addChild(R1);
	R0->addChild(R2);

	R1->addChild(R0);

	R2->addChild(R1);
	R2->addChild(R2);

	R4->addChild(R3);
	R4->addChild(R4);

	Graph graph2 = Graph();
	graph2.addNode(R0);
	graph2.addNode(R1);
	graph2.addNode(R2);
	graph2.addNode(R3);
	graph2.addNode(R4);

	cout << graph2.toString() << '\n';

	cout << "BEGIN TEST 6: Compute SCCs\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";
	cout << "Reverse Graph:\n";
	graph2.reverse();
	cout << graph2.toString() << '\n';


	cout << "Postorder: \n";
	cout << graph2.postOrder();

	cout << "\nSCCs: \n";
	std::vector<std::vector<Node*> >* SCCs = graph2.getSCCs();

	for (auto scc : *(SCCs)) {
		for (auto node : scc) {
			cout << node->toString() << ',';
		}
		cout << '\n';
	}

	cout << "------------------------------------------------------------------------------------------------------------\n";
	//	empty graph


	/*
	 * END TEST 6
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 7
	 *
	 *
	 *
	 *
	 *
	 */

//	R0:
//	R1:
//	R2:
//	R3:R0,R1,R2
//	R4:R3

	R0 = new Node(0);
	R1 = new Node(1);
	R2 = new Node(2);
	R3 = new Node(3);
	R4 = new Node(4);


	R3->addChild(R0);
	R3->addChild(R1);
	R3->addChild(R2);

	R4->addChild(R3);

	graph2 = Graph();
	graph2.addNode(R0);
	graph2.addNode(R1);
	graph2.addNode(R2);
	graph2.addNode(R3);
	graph2.addNode(R4);

	cout << "BEGIN TEST 7: Compute More SCCs\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";


	cout << "New Graph: \n" << graph2.toString() << '\n';
	graph2.reverse();

	cout << "SCCs: \n";
	SCCs = graph2.getSCCs();

	for (auto scc : *(SCCs)) {
		for (auto node : scc) {
			cout << node->toString() << ',';
		}
		cout << '\n';
	}

	cout << "------------------------------------------------------------------------------------------------------------\n";


	/*
	 * END TEST 7
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 8
	 *
	 *
	 *
	 *
	 *
	 */

	R0 = new Node(0);
	R1 = new Node(1);
	R2 = new Node(2);
	R3 = new Node(3);
	R4 = new Node(4);


	graph2 = Graph();
	graph2.addNode(R0);
	graph2.addNode(R1);
	graph2.addNode(R2);
	graph2.addNode(R3);
	graph2.addNode(R4);

	cout << "BEGIN TEST 8: Compute SCCs for forest of single nodes (should just be the list of nodes)\n";

	cout << "------------------------------------------------------------------------------------------------------------\n";

	cout << "New Graph: \n" << graph2.toString() << '\n';
	graph2.reverse();

	cout << "SCCs: \n";
	SCCs = graph2.getSCCs();

	for (auto scc : *(SCCs)) {
		for (auto node : scc) {
			cout << node->toString() << ',';
		}
		cout << '\n';
	}

	cout << "------------------------------------------------------------------------------------------------------------\n";


	/*
	 * END TEST 8
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 9
	 *
	 *
	 *
	 *
	 *
	 */

	R0 = new Node(0);
	R1 = new Node(1);
	R2 = new Node(2);
	R3 = new Node(3);
	R4 = new Node(4);


	graph2 = Graph();
	graph2.addNode(R0);
	graph2.addNode(R1);
	graph2.addNode(R2);
	graph2.addNode(R3);
	graph2.addNode(R4);

	cout << "BEGIN TEST 9: Compute SCCs for forest of single nodes (should just be the list of nodes)\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";

	cout << "New Graph: \n" << graph2.toString() << '\n';
	graph2.reverse();

	cout << "SCCs: \n";
	SCCs = graph2.getSCCs();

	for (auto scc : *(SCCs)) {
		for (auto node : scc) {
			cout << node->toString() << ',';
		}
		cout << '\n';
	}
	cout << "------------------------------------------------------------------------------------------------------------\n";


	/*
	 * END TEST 9
	 *
	 *
	 *
	 *
	 *
	 */

	/*
	 * START TEST 10
	 *
	 *
	 *
	 *
	 *
	 */

	graph2 = Graph();
	cout << "BEGIN TEST 10: Guarding against empty graph.\n";
	cout << "------------------------------------------------------------------------------------------------------------\n";

	cout << "New Graph: \n" << graph2.toString() << '\n';
	graph2.reverse();

	cout << "SCCs: \n";
	SCCs = graph2.getSCCs();

	for (auto scc : *(SCCs)) {
		for (auto node : scc) {
			cout << node->toString() << ',';
		}
		cout << '\n';
	}
	cout << "------------------------------------------------------------------------------------------------------------\n";

	cout << "SUCCESS!";

	/*
	 * END TEST 10
	 *
	 *
	 *
	 *
	 *
	 */
	return 0;
}
