/*
 * Node.h
 *
 *  Created on: Dec 5, 2016
 *      Author: drewctate
 */

#ifndef NODE_H_
#define NODE_H_

#include <iomanip>
#include <vector>
#include <sstream>

class Node {
public:
	Node(int val_in);
	virtual ~Node();

	int getVal();
	void setTraversalNum(int num);
	int getTraversalNum();
	void addChild(Node*);
	std::vector<Node*>* getChildren();
	void removeChild(int val);
	void clearChildren();
	std::string toString();

private:
	int val;
	std::vector<Node*> children;
	int postTraversalNumber = -1;
};

#endif /* NODE_H_ */
