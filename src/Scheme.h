/*
 * Scheme.h
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#ifndef SCHEME_H_
#define SCHEME_H_

#include <list>
#include <sstream>
#include "token.h"

class Scheme {
public:
	Scheme();
	virtual ~Scheme();

	token getName();
	void setName(token);

	std::list<token>* getIds();
	void addId(token);
	void addIds(std::list<token>&);

	std::string toString();

private:
	token name;
	std::list<token>* idList;
	int idCount;
};

#endif /* SCHEME_H_ */
