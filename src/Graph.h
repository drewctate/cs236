/*
 * Graph.h
 *
 *  Created on: Dec 5, 2016
 *      Author: drewctate
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include "Node.h"
#include <sstream>
#include <stack>
#include <set>
#include <utility>

class Graph {
public:
	Graph();
	virtual ~Graph();
	void addNode(Node* node);
	std::vector<Node*> getNodes();
	std::string reachableFrom(int start);
	std::string postOrder();
	std::string postOrder(int start);
	Node* getNodeWVal(int val);
	std::vector<std::vector<Node*> >* getSCCs();
	std::string toString();
	void reverse();

private:
	Node* depthSearch(Node* node, int val);
	void genRecursiveString(Node* node, std::stringstream& stream);
	void postOrder(Node* node, std::stringstream& stream);
	std::vector<Node*> nodes;
	std::stack<Node*> postStack;
	std::set<Node*> visited;
	void reverse(Node*& parent);
	std::vector<Node*> getSCC(Node* node);
	void getSCCRec(Node* node, std::vector<Node*>& scc);
	int postOrderCount;
};

#endif /* GRAPH_H_ */
