/*
 * token.cpp
 *
 *  Created on: Sep 22, 2016
 *      Author: drewctate
 */

#include "token.h"

token::token(std::string type_in, std::string value_in, int lineNum_in) :
type(type_in), value(value_in), lineNum(lineNum_in) {}

token::~token() {
	// TODO Auto-generated destructor stub
}

std::string token::getType() {
	return type;
}

std::string token::getValue() {
	return value;
}

int token::getLineNum() {
	return lineNum;
}

std::string token::toString() {
	std::stringstream sstream;
	sstream << "(" << this->getType() << ",\"" << this->getValue() << "\"," << this->getLineNum() << ")\n";
	return sstream.str();
}
