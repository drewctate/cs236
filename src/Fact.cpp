/*
 * Fact.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#include "Fact.h"

Fact::Fact() :
name(token("ID", "uninitialized", 0)), strList(new std::list<token>()), strCount(0) {}

Fact::~Fact() {}

token Fact::getName() {
	return name;
}

void Fact::setName(token t) {
	name = t;
}

std::list<token>* Fact::getStrs() {
	return strList;
}

void Fact::addStr(token t) {
	strList->push_back(t);
	strCount++;
}

void Fact::addStrs(std::list<token>& lst) {
	for (std::list<token>::iterator it = lst.begin(); it != lst.end(); it++) {
		addStr(*it);
	}
}

std::string Fact::toString() {
	std::stringstream stream;
	stream << name.getValue() << '(';
	int count = 0;
	for (std::list<token>::iterator it = strList->begin(); it != strList->end(); it++) {
		stream << it->getValue();
		if (count < strCount - 1) {
			stream << ',';
		}
		count++;
	}
	stream << ").";
	return stream.str();
}
