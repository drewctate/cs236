/*
 * Graph.cpp
 *
 *  Created on: Dec 5, 2016
 *      Author: drewctate
 */

#include "Graph.h"
#include <iostream>

Graph::Graph() : postOrderCount(0) {}

Graph::~Graph() {
	nodes.clear();
}

void Graph::addNode(Node* node) {
	nodes.push_back(node);
}

std::vector<Node*> Graph::getNodes() {
	return nodes;
}

std::string Graph::reachableFrom(int val) {
	return postOrder(val);
}

Node* Graph::getNodeWVal(int val) {
	for (Node* node : nodes) {
		if (val == node->getVal()) {
			return node;
		}
	}
	return NULL;
}

std::vector<std::vector<Node*> >* Graph::getSCCs() {
	std::vector<std::vector<Node*> >* SCCs = new std::vector<std::vector<Node*> >();
	for (Node* node : nodes) {
		if (visited.find(node) == visited.end()) {
			SCCs->push_back(getSCC(node));
		}
	}
	return SCCs;
}

std::vector<Node*> Graph::getSCC(Node* node) {
	std::vector<Node*> scc;
	getSCCRec(node, scc);
	return scc;
}

void Graph::getSCCRec(Node* node, std::vector<Node*>& scc) {
	if (std::get<1>(visited.insert(node))) { // if not already visited
		std::vector<Node*> children = *(node->getChildren());
		for (auto child : children) {
			getSCCRec(child, scc);
		}
		scc.push_back(node);
	}
}

Node* Graph::depthSearch(Node* node, int val) {
	Node* ret = NULL;

	if (node->getVal() == val) {
		return node;
	} else {
		std::vector<Node*> children = *(node->getChildren());
		for (auto child : children) {
			ret = depthSearch(child, val);
			if (ret) {
				return ret;
			}
		}
	}

	return NULL;
}

std::string Graph::postOrder() {
	std::stringstream stream;
	for (Node* node : nodes) {
		if (visited.find(node) == visited.end()) {
			stream << postOrder(node->getVal());
		}
	}
	postOrderCount = 0;
	visited.clear();
	return stream.str();
}

std::string Graph::postOrder(int start) {
	std::stringstream stream;
	Node* curRoot = getNodeWVal(start);

	if (!curRoot) {
		return "Not found!";
	}

	postOrder(curRoot, stream);

	int size = postStack.size();
	while (size--) {
		postStack.top()->setTraversalNum(postOrderCount);
		stream << postStack.top()->toString() << ": " << postOrderCount << '\n';
		postStack.pop();
		postOrderCount++;
	}
	return stream.str();
}

void Graph::postOrder(Node* node, std::stringstream& stream) {
	if (std::get<1>(visited.insert(node))) { // if not already visited
		std::vector<Node*> children = *(node->getChildren());
		postStack.push(node);
		for (auto child : children) {
			postOrder(child, stream);
		}
	}
}

void Graph::reverse(Node*& node) {

}

void Graph::reverse() {
	std::vector<std::vector<Node*> > childrenLists;
	for (auto node : nodes) {
		childrenLists.push_back(*(node->getChildren()));
	}

	for (int i = 0; i < nodes.size(); i++) {
		for (auto child : childrenLists[i]) {
			nodes[i]->removeChild(child->getVal());
			child->addChild(nodes[i]);
		}
	}
}

std::string Graph::toString() {
	std::stringstream stream;
	for (Node* node : nodes) {
		stream << node->toString() << ": ";
		for (Node* child : *(node->getChildren())) {
			stream << child->toString() << ' ';
		}
		stream << '\n';
	}
	return stream.str();
}

