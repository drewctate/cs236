/*
 * Scheme.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#include "Scheme.h"

Scheme::Scheme() :
name(token("ID", "uninitialized", 0)), idList(new std::list<token>()), idCount(0) {}

Scheme::~Scheme() {}

token Scheme::getName() {
	return name;
}

void Scheme::setName(token t) {
	name = t;
}

std::list<token>* Scheme::getIds() {
	return idList;
}

void Scheme::addId(token t) {
	idList->push_back(t);
	idCount++;
}

void Scheme::addIds(std::list<token>& lst) {
	for (std::list<token>::iterator it = lst.begin(); it != lst.end(); it++) {
		addId(*it);
	}
}

std::string Scheme::toString() {
	std::stringstream stream;
	stream << name.getValue() << "(";
	int count = 0;
	for (std::list<token>::iterator it = idList->begin(); it != idList->end(); it++) {
		stream << it->getValue();
		if (count < idCount - 1) {
			stream << ',';
		}
		count++;
	}
	stream << ")";
	return stream.str();
}
