/*
 * Database.cpp
 *
 *  Created on: Nov 1, 2016
 *      Author: drewctate
 */

#include "Database.h"

Database::Database(DatalogProgram& dp_in) : dp(dp_in) {
	buildRelations();
	populateRelations();
}

Database::~Database() {
}

void Database::buildRelations() {
	for (Scheme sc : *(dp.getSchemes())) {
		std::list<std::string> newSchema;
		std::list<token> ids = *(sc.getIds());
		for (std::list<token>::iterator it = ids.begin(); it != ids.end(); it++) {
			newSchema.push_back((*it).getValue());
		}
		relations.push_back(Relation((sc.getName()).getValue(), newSchema));
	}
}

void Database::populateRelations() {
	for (std::list<Relation>::iterator it = relations.begin(); it != relations.end(); it++) {
		for (Fact f : *(dp.getFacts())) {
			if ((f.getName()).getValue() == it->getName()) {
				std::vector<std::string> newTuple;
				for (token t : *(f.getStrs())) {
					newTuple.push_back(t.getValue());
				}
				it->addTuple(newTuple);
			}
		}
	}
}



Relation Database::runQuery(Predicate q) {
	std::list<std::string> toProject;
	std::map<std::string,std::string> toRename;
	std::map<std::string,std::string> toSelect;
	Relation* rel = NULL;
	for (std::list<Relation>::iterator it = relations.begin(); it != relations.end(); it++) {
		if (it->getName() == (q.getName()).getValue()) {
			rel = &(*it);
			break;
		}
	}

	Relation curRel = *rel;

	std::list<Parameter*>* params = q.getParams();
	std::map<std::string, std::vector<int> > paramPositions;

	int paramPos = 0;
	for (Parameter* p : *params) {
		if ((p->getData())[0] != '\'') { // ID
			toProject.push_back(curRel.getSchemaId(paramPos));
			(paramPositions[p->getData()]).push_back(paramPos);
			if (p->getData() != curRel.getSchemaId(paramPos)) {
				toRename[curRel.getSchemaId(paramPos)] = p->getData();
			}
		}
		else {
			toSelect[curRel.getSchemaId(paramPos)] = p->getData();
		}
		paramPos++;
	}

	std::list<std::string> toProjectMulti;
	for (std::pair<std::string,std::vector<int> > s : paramPositions) {
//		if ((s.second).size() > 1) {
			curRel = curRel.selectIds(s.second);
			toProjectMulti.push_back(curRel.getSchemaId((s.second)[0]));
//		}
	}

	curRel = curRel.select(toSelect);

	curRel = curRel.project(toProject);
	std::cout << toProjectMulti.size() << '\n';
	if (toProjectMulti.size()) {
		curRel = curRel.project(toProjectMulti);
	}
	curRel = curRel.rename(toRename);

	return curRel;
}

std::string Database::formatQueryResults(Predicate q, Relation rel) {
	std::stringstream stream;
	stream << q.toString() << "? ";
	if (!rel.getSize()) {
		stream << "No\n";
	}
	else {
		stream << "Yes(" << rel.getSize() << ")\n";
	}
	int numCols = rel.getNumCols();
	if (numCols) {
		for (int j = 0; j < rel.getSize(); j++) {
			stream << "  ";
			for (int i = 0; i < numCols; i++) {
				stream << rel.getSchemaId(i) << "=" << rel.access(j, i);
				if (i < numCols - 1) stream << ", ";
			}
			stream << '\n';
		}
	}
	return stream.str();
}

std::string Database::runQueries() {
	std::stringstream stream;
	stream << "Schemes populated after " << rulePasses << " passes through the Rules.\n";
	for (Predicate q : *(dp.getQueries())) {
		Relation rel = runQuery(q);
		stream << formatQueryResults(q, rel);
	}
	return stream.str();
}

void Database::evalRules() {
	bool tuplesAdded = true;
	rulePasses = 0;
	while (tuplesAdded) {
		bool added = false;
		rulePasses++;
		for (Rule rule : *(dp.getRules())) {
			std::list<std::string> emptySchema;
			std::list<Predicate> preds = *(rule.getPredicates());
			std::list<Predicate>::iterator it = preds.begin();

			// Join relations corresponding to the predicates
			Relation joinedPredicates = runQuery(*it);
			it++;
			while (it != preds.end()) {
				joinedPredicates = joinedPredicates.naturalJoin(runQuery(*it));
				it++;
			}


			// Project the columns defined in the head predicate
			std::list<std::string> toProject;
			for (token id : *(rule.getHeadPredicate()).getIds()) {
				toProject.push_back(id.getValue());
			}

//			for (auto id : toProject) {
//				std::cout << id;
//			}
//			std::cout << '\n';
			Relation result = joinedPredicates.project(toProject);


			// Find relation corresponding to head predicate
			result.setName(((rule.getHeadPredicate()).getName()).getValue());
			std::list<Relation>::iterator toAddTo = relations.begin();
			for (toAddTo = relations.begin(); toAddTo != relations.end(); toAddTo++) {
				if (toAddTo->getName() == result.getName()) {
					break;
				}
			}

			std::map<std::string,std::string> toRename;
			for (int i = 0; i < toAddTo->getNumCols(); i++) {
				toRename[result.getSchemaId(i)] = toAddTo->getSchemaId(i);
			}

			result = result.rename(toRename);
//			std::cout << result.toString();

			int beforeSize = toAddTo->getSize();
			result = toAddTo->unionWith(result);
			int afterSize = result.getSize();

			if (beforeSize != afterSize) {
				added = true;
			}

			toAddTo = relations.erase(toAddTo);
			relations.insert(toAddTo, result);
		}
		if (!added) {
			tuplesAdded = false;
		}
	}
}

std::string Database::toString() {
	std::stringstream stream;
	for (Relation r : relations) {
		stream << r.toString();
	}
	return stream.str();
}
