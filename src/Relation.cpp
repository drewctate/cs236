/*
 * Relation.cpp
 *
 *  Created on: Oct 25, 2016
 *      Author: drewctate
 */

#include "Relation.h"

Relation::Relation(std::string name_in, std::list<std::string>& schema_in) :
name(name_in), schema(schema_in) {}

Relation::~Relation() {
	clear();
}

void Relation::clear() {
	schema.clear();
	tuples.clear();
	name = "";
}

std::string Relation::getName() {
	return name;
}

void Relation::setName(std::string newName) {
	name = newName;
}

void Relation::setSchema(std::list<std::string>& newSchema) {
	schema = newSchema;
}

Relation Relation::rename(std::map<std::string,std::string>& nameList) {
	std::list<std::string> newSchema;
	bool found;

	for (std::list<std::string>::iterator it = schema.begin(); it != schema.end(); it++) {
		found = false;
		for (auto name : nameList) {
			if (*it == name.first) {
				newSchema.push_back(name.second);
				found = true;
			}
		}
		if (!found) {
			newSchema.push_back(*it);
		}
	}
	Relation ret = Relation(name, newSchema);
	ret.addTuples(tuples);
	return ret;
}

Relation Relation::select(std::map<std::string,std::string>& attrs) {
	std::list<std::string> emptySchemes;
	Relation rel = *this;
	for (auto attr : attrs) {
		if ((attr.second)[0] == '\'')
			rel = rel.selectStr(attr.first, attr.second);
	}
	return rel;
}

Relation Relation::selectIds(std::vector<int>& positions) {
	//  begin selection operation
	std::set< std::vector<std::string> > newTuples;
	for (std::set< std::vector<std::string> >::iterator it = tuples.begin(); it != tuples.end(); it++) {
		bool add = true;
		for (int i = 0; i < positions.size(); i++) {
			if (!((*it)[positions[0]] == (*it)[positions[i]])) {
				add = false;
			}
		}
		if (add)
			newTuples.insert(*it);
	}
	Relation ret = Relation(name, schema);
	ret.addTuples(newTuples);
	return ret;
}

Relation Relation::selectStr(std::string attr, std::string val) {
	int attrPosition = -1;
	int count = 0;
	for (std::list<std::string>::iterator it = schema.begin(); it != schema.end(); it++) {
		if (*it == attr) {
			attrPosition = count;
			break;
		}
		count++;
	}

	if (attrPosition == -1) {  // attribute not found in schema
		return *this;
	}

	//  begin selection operation
	std::set< std::vector<std::string> > newTuples;
	for (std::set< std::vector<std::string> >::iterator it = tuples.begin(); it != tuples.end(); it++) {
		if ((*it)[attrPosition] == val) {
			newTuples.insert(*it);
		}
	}
	Relation ret = Relation(name, schema);
	ret.addTuples(newTuples);
	return ret;
}

Relation Relation::project(std::list<std::string>& attrs) {
	std::list<int> attrPositions;
	std::list<std::string> newSchema;

	for (auto attr : attrs) {
		int count = 0;
		for (std::list<std::string>::iterator it = schema.begin(); it != schema.end(); it++) {
			if (*it == attr) {
				newSchema.push_back(*it);
				attrPositions.push_back(count);
				break;
			}
			count++;
		}
	}

	std::set< std::vector<std::string> > newTuples;

	for (std::set< std::vector<std::string> >::iterator it = tuples.begin(); it != tuples.end(); it++) {
		std::vector<std::string> newTuple;
		for (int attrPos : attrPositions) {
			newTuple.push_back((*it)[attrPos]);
		}
		newTuples.insert(newTuple);
	}

	Relation ret = Relation(name, newSchema);
	ret.addTuples(newTuples);
	return ret;
}

Relation Relation::unionWith(Relation r) {
	if (r.getNumCols() != getNumCols()) {
		return *this;
	}
	bool identical = true;
	int count = 0;
	for (std::string id : schema) {
		if (id != r.getSchemaId(count)) {
			identical = false;
		}
		count++;
	}
	if (!identical) {
		return *this;
	}

	Relation ret = Relation(name, schema);
	ret.addTuples(tuples);
	std::set< std::vector<std::string> > newTuples = r.getTuples();
	ret.addTuples(newTuples);
	return ret;
}

Relation Relation::naturalJoin(Relation r) {
	std::map<int,int> inCommon;
	for (int i = 0; i < getNumCols(); i++) {
		for (int j = 0; j < r.getNumCols(); j++) {
			if (getSchemaId(i) == r.getSchemaId(j)) {
				inCommon[i] = j;
			}
		}
	}

	std::list<std::string> fullSchema = schema;
	Relation ret = Relation(name, schema);
	for (int i = 0; i < r.getNumCols(); i++) {
		bool add = true;
		for (auto pair : inCommon) {
			if (pair.second == i) {
				add = false;
			}
		}
		if (add) {
			fullSchema.push_back(r.getSchemaId(i));
		}
	}
	ret.setSchema(fullSchema);

	if (inCommon.size()) {
		for (auto tuple1 : tuples) {
			std::vector<std::string> newTuple;
			for (auto tuple2 : r.getTuples()) {
				bool matched = true;
				for (auto match : inCommon) {
					if (tuple1[match.first] != tuple2[match.second]) {
						matched = false;
					}
				}
				if (matched) {
					ret.addTuple(mergeTuples(tuple1, tuple2, inCommon));
				}
			}
		}
	}
	else {  // no common attributes, generate cartesian product
		for (auto tuple1 : tuples) {
			std::vector<std::string> newTuple;
			for (auto tuple2 : r.getTuples()) {
				ret.addTuple(mergeTuples(tuple1, tuple2, inCommon));
			}
		}
	}

	return ret;
}

std::vector<std::string> Relation::mergeTuples(std::vector<std::string> tuple1,
		std::vector<std::string> tuple2, std::map<int,int> inCommon) {
	for (int i = 0; i < tuple2.size(); i++) {
		bool add = true;
		for (auto pair : inCommon) {
			if (i == pair.second) {
				add = false;
			}
		}
		if (add) {
			tuple1.push_back(tuple2[i]);
		}
	}
	return tuple1;
}

int Relation::getSize() {
	return tuples.size();
}

int Relation::getNumCols() {
	return schema.size();
}

std::string Relation::access(int i, int j) {
	int count = 0;
	std::string ret;
	for (auto tuple : tuples) {
		if (count == i)
			ret = tuple[j];
		count++;
	}
	return ret;
}

std::string Relation::getSchemaId(int position) {
	int count = 0;
	std::string ret;
	for (std::list<std::string>::iterator it = schema.begin(); it != schema.end(); it++) {
		if (position == count) {
			ret = *it;
			break;
		}
		count++;
	}
	return ret;
}

bool Relation::addTuple(const std::vector<std::string>& tuple) {
	return std::get<1> (tuples.insert(tuple));
}

void Relation::addTuples(std::set< std::vector<std::string> >& s) {
	for (std::set< std::vector<std::string> >::iterator it = s.begin(); it != s.end(); it++) {
		addTuple(*it);
	}
}

std::set< std::vector<std::string> > Relation::getTuples() {
	return tuples;
}

std::string Relation::toString() {
	std::stringstream stream;
	stream << "Name: " << name << std::endl;
	std::string line;
	for (std::list<std::string>::iterator it = schema.begin(); it != schema.end(); it++) {
		stream << std::setw(20) << *it;
		line += "--------------------";
	}
	stream << std::endl << line << std::endl;

	for (std::set< std::vector<std::string> >::iterator it = tuples.begin(); it != tuples.end(); it++) {
		std::vector<std::string> v = *it;
		for (std::string val : v) {
			stream << std::setw(20) << val;
		}
		stream << std::endl;
	}
	stream << line << std::endl;
	return stream.str();
}

