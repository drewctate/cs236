/*
 * HeadPredicate.h
 *
 *  Created on: Oct 6, 2016
 *      Author: drewctate
 */

#ifndef HEADPREDICATE_H_
#define HEADPREDICATE_H_

#include <list>
#include "token.h"

class HeadPredicate {
public:
	HeadPredicate();
	virtual ~HeadPredicate();

	token getName();
	void setName(token);

	std::list<token>* getIds();
	void addId(token);
	void addIds(std::list<token>&);

	std::string toString();

private:
	token name;
	std::list<token>* idList;
	int idCount;
};

#endif /* HEADPREDICATE_H_ */
