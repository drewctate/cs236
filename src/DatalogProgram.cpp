/*
 * DatalogProgram.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#include "DatalogProgram.h"
#include <iostream>


DatalogProgram::DatalogProgram() :
schemes(new std::list<Scheme>()),facts(new std::list<Fact>()),
rules(new std::list<Rule>()), queries(new std::list<Predicate>()), domain(new std::set<std::string>()) {}

DatalogProgram::~DatalogProgram() {
	schemes->clear();
	facts->clear();
	rules->clear();
	queries->clear();
}

std::list<Scheme>* DatalogProgram::getSchemes() {
	return schemes;
}

void DatalogProgram::addScheme(Scheme& scheme) {
	schemes->push_back(scheme);
}

std::list<Fact>* DatalogProgram::getFacts() {
	return facts;
}

void DatalogProgram::addFact(Fact& fact) {
	facts->push_back(fact);
	std::list<token>* strs = fact.getStrs();
	for (std::list<token>::iterator it = strs->begin(); it != strs->end(); it++) {
		domain->insert(it->getValue());
	}
}

std::list<Rule>* DatalogProgram::getRules() {
	return rules;
}

void DatalogProgram::addRule(Rule rule) {
	rules->push_back(rule);
}

std::list<Predicate>* DatalogProgram::getQueries() {
	return queries;
}

void DatalogProgram::addQuery(Predicate& query) {
	queries->push_back(query);
}

std::string DatalogProgram::toString() {
	std::stringstream stream;
	stream << "Schemes(" << schemes->size() << "):\n";
	for (std::list<Scheme>::iterator it = schemes->begin(); it != schemes->end(); it++) {
		stream << "  " << it->toString() << std::endl;
	}

	stream << "Facts(" << facts->size() << "):\n";
	for (std::list<Fact>::iterator it = facts->begin(); it != facts->end(); it++) {
		stream << "  " << it->toString() << std::endl;
	}

	stream << "Rules(" << rules->size() << "):\n";
	for (std::list<Rule>::iterator it = rules->begin(); it != rules->end(); it++) {
		stream << "  " << it->toString() << std::endl;
	}

	stream << "Queries(" << queries->size() << "):\n";
	for (std::list<Predicate>::iterator it = queries->begin(); it != queries->end(); it++) {
		stream << "  " << it->toString() << '?' << std::endl;
	}

	stream << "Domain(" << domain->size() << "):\n";
	for (std::set<std::string>::iterator it = domain->begin(); it != domain->end(); it++) {
		stream << "  " << *it << std::endl;
	}

	return stream.str();
}
