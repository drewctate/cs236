/*
 * bool lexer.cpp
 *
 *  Created on: Sep 212016
 *      Author: drewctate
 */

#include "lexer.h"
#include <iostream>

lexer::lexer(std::list<char> &toParse_in) :
toParse(toParse_in), it(toParse.begin()), fileLength(toParse.size()), beginLineNum(0), offset(0), lineNum(1) {}

lexer::~lexer() {
	tokens.clear();
	toParse.clear();
}

bool lexer::analyze() {
	while (genNextTok() && !isEOF());
	tokens.push_back(token("EOF", "", lineNum));
	return false;
}

bool lexer::genNextTok() {
	while(isspace(*it)) {
		if (*it == '\n') lineNum++;
		it++;
	}

	switch (*it) {
		case '_':
			return false;
		case ',':
			tokens.push_back(token("COMMA", ",", lineNum));
			it++;
			return true;
		case '.':
			tokens.push_back(token("PERIOD", ".", lineNum));
			it++;
			return true;
		case '?':
			tokens.push_back(token("Q_MARK", "?", lineNum));
			it++;
			return true;
		case '(':
			tokens.push_back(token("LEFT_PAREN", "(", lineNum));
			it++;
			return true;
		case ')':
			tokens.push_back(token("RIGHT_PAREN", ")", lineNum));
			it++;
			return true;
		case ':':
			it++;
			if (*it != '-')
				tokens.push_back(token("COLON", ":", lineNum));
			else {
				tokens.push_back(token("COLON_DASH", ":-", lineNum));
				it++;
			}
			return true;
		case '*':
			tokens.push_back(token("MULTIPLY", "*", lineNum));
			it++;
			return true;
		case '+':
			tokens.push_back(token("ADD", "+", lineNum));
			it++;
			return true;
		default:
			if (!isReservedWd()){
				rewind();
				return isOther();
			}
			return true;
	}
}

bool lexer::isReservedWd() {
	if (isSchemes()) {
		tokens.push_back(token("SCHEMES", "Schemes", lineNum));
		return true;
	}
	else if (isFacts()) {
		tokens.push_back(token("FACTS", "Facts", lineNum));
		return true;
	}
	else if (isRules()) {
		tokens.push_back(token("RULES", "Rules", lineNum));
		return true;
	}
	else if (isQueries()) {
		tokens.push_back(token("QUERIES", "Queries", lineNum));
		return true;
	}
	else {
		return false;
	}
}

bool lexer::isOther() {
	if (isId()) {
		return true;
	}
	else if (isString()) {
		return true;
	}
	else if (isComment()) {
		return true;
	}
	return undefined();
}

bool lexer::finalChar(char ch) {
	if (*it == ch) {
		it++;
		offset++;
		if (isalpha(*it)) {
			rewind();
			return false;
		}
		else {
			offset = 0;
			return true;
		}
	}
	rewind();
	return false;
}


void lexer::rewind() {
	for (int i = 0; i < offset; i++)
		it--;
	offset = 0;
}

//SCHEMES

bool lexer::isSchemes() {
	if (*it == 'S') {
		it++;
		offset++;
		return sch_c();
	}
	return false;
}

bool lexer::sch_c() {
	if (*it == 'c') {
		it++;
		offset++;
		return sch_h();
	}
	return false;
}

bool lexer::sch_h() {
	if (*it == 'h') {
		it++;
		offset++;
		return sch_e();
	}
	return false;
}

bool lexer::sch_e() {
	if (*it == 'e') {
		it++;
		offset++;
		return sch_m();
	}
	return false;
}

bool lexer::sch_m() {
	if (*it == 'm') {
		it++;
		offset++;
		return sch_e2();
	}
	return false;
}

bool lexer::sch_e2() {
	if (*it == 'e') {
		it++;
		offset++;
		return finalChar('s');
	}
	return false;
}

//END_SCHEMES

//FACTS

bool lexer::isFacts() {
	if (*it == 'F') {
		it++;
		offset++;
		return fac_a();
	}
	return false;
}

bool lexer::fac_a() {
	if (*it == 'a') {
		it++;
		offset++;
		return fac_c();
	}
	return false;
}

bool lexer::fac_c() {
	if (*it == 'c') {
		it++;
		offset++;
		return fac_t();
	}
	return false;
}

bool lexer::fac_t() {
	if (*it == 't') {
		it++;
		offset++;
		return finalChar('s');
	}
	return false;
}

//END_FACTS

//RULES

bool lexer::isRules() {
	if (*it == 'R') {
		it++;
		offset++;
		return rul_u();
	}
	return false;
}

bool lexer::rul_u() {
	if (*it == 'u') {
		it++;
		offset++;
		return rul_l();
	}
	return false;
}

bool lexer::rul_l() {
	if (*it == 'l') {
		it++;
		offset++;
		return rul_e();
	}
	return false;
}

bool lexer::rul_e() {
	if (*it == 'e') {
		it++;
		offset++;
		return finalChar('s');
	}
	return false;
}

//END_RULES

//QUERIES

bool lexer::isQueries() {
	if (*it == 'Q') {
		it++;
		offset++;
		return que_u();
	}
	return false;
}

bool lexer::que_u() {
	if (*it == 'u') {
		it++;
		offset++;
		return que_e();
	}
	return false;
}

bool lexer::que_e() {
	if (*it == 'e') {
		it++;
		offset++;
		return que_r();
	}
	return false;
}

bool lexer::que_r() {
	if (*it == 'r') {
		it++;
		offset++;
		return que_i();
	}
	return false;
}

bool lexer::que_i() {
	if (*it == 'i') {
		it++;
		offset++;
		return que_e2();
	}
	return false;
}

bool lexer::que_e2() {
	if (*it == 'e') {
		it++;
		offset++;
		return finalChar('s');
	}
	return false;
}

//END_QUERIES

bool lexer::isId() {
	if (isalpha(*it)) {
		sstream << *it;
		it++;
		offset++;
		while (isalpha(*it) || isdigit(*it)) {
			sstream << *it;
			it++;
			offset++;
		}
		tokens.push_back(token("ID", sstream.str(), lineNum));
		offset = 0;
		sstream.str("");
		return true;
	}
	return false;
}

bool lexer::isString() {
	if (*it == '\'') {
		beginLineNum = lineNum;
		bool cont = true;
		while (cont) {
			sstream << *it;
			it++;
			offset++;
			while (*it != '\'') {
				if (isEOF()) return false;
				sstream << *it;
				if (*it == '\n') lineNum++;
				it++;
				offset++;
			}
			sstream << *it;
			it++;
			offset++;
			if (!(*it == '\'')) cont = false;
		}
		offset = 0;
		tokens.push_back(token("STRING", sstream.str(), beginLineNum));
		sstream.str("");
		return true;
	}
	return false;
}

bool lexer::isComment() {
	if (*it != '#') {
		return false;
	}
	else {
		sstream << *it;
		offset++;
		it++;
		if (*it == '|') {
			sstream << *it;
			offset++;
			it++;
			return isMultiLineComment();
		}
		while (*it != '\n' && !isEOF())  {
			sstream << *it;
			offset++;
			it++;
		}
		offset = 0;
		it++;
		tokens.push_back(token("COMMENT", sstream.str(), lineNum));
		lineNum++;
		sstream.str("");
		return true;
	}
}

bool lexer::isMultiLineComment() {
	beginLineNum = lineNum;
	while (*it != '|') {
		if (isEOF()) return false;
		if (*it == '\n') lineNum++;
		sstream << *it;
		offset++;
		it++;
	}
	sstream << *it;
	offset++;
	it++;
	if (*it != '#') return false;
	sstream << *it;
	offset = 0;
	it++;
	tokens.push_back(token("COMMENT", sstream.str(), beginLineNum));
	sstream.str("");
	return true;
}

bool lexer::undefined() {
	if (!offset) {
		if (isEOF()) return false;
		sstream << *it;
		tokens.push_back(token("UNDEFINED", sstream.str(), lineNum));
		it++;
		sstream.str("");
	}
	else {
		tokens.push_back(token("UNDEFINED", sstream.str(), beginLineNum));
		sstream.str("");
	}
	return true;
}

bool lexer::isEOF() {
	return it == toParse.end() || *it == '_';
}

std::list<token>* lexer::getTokens() {
	return &tokens;
}
