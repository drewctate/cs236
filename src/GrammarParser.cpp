/*
 * GrammarParser.cpp
 *
 *  Created on: Oct 2, 2016
 *      Author: drewctate
 */

#include "GrammarParser.h"

GrammarParser::GrammarParser(std::list<token>& tokens_in) :
tokens(tokens_in), it(tokens.begin()), dp(new DatalogProgram()), success(false) {}

GrammarParser::~GrammarParser() {
	tokens.clear();
}

bool GrammarParser::hadSuccess() {
	return success;
}

DatalogProgram* GrammarParser::getProgram() {
	return dp;
}

void GrammarParser::parse() {
	removeComments();
	try {
		match(SCHEMES);
		match(COLON);
		match(SCHEME);
		match(SCHEMELIST);

		match(FACTS);
		match(COLON);
		match(FACTLIST);

		match(RULES);
		match(COLON);
		match(RULELIST);

		match(QUERIES);
		match(COLON);
		match(QUERY);
		match(QUERYLIST);

		match(END);
		success = true;
	} catch (token &t) {
		std::cout << "Failure!" << "\n  " << t.toString();
	}
}

void GrammarParser::match(grammar_t type) {
	switch (type) {
		case COLON:
			matchColon();
			break;
		case SCHEMES:
			matchSchemes();
			break;
		case SCHEME:
			matchScheme();
			break;
		case SCHEMELIST:
			matchSchemeList();
			break;
		case FACTS:
			matchFacts();
			break;
		case FACTLIST:
			matchFactList();
			break;
		case RULES:
			matchRules();
			break;
		case RULELIST:
			matchRuleList();
			break;
		case QUERIES:
			matchQueries();
			break;
		case QUERY:
			matchQuery();
			break;
		case QUERYLIST:
			matchQueryList();
			break;
		case END:
			matchEnd();
			break;
	}
}

void GrammarParser::printTokens() {
	for (std::list<token>::iterator it = tokens.begin(); it != tokens.end(); it++) {
		std::cout << it->toString();
	}
}

void GrammarParser::removeComments() {
	std::list<token>::iterator it_local = tokens.begin();
	while(it_local != tokens.end()) {
		if (it_local->getType() == "COMMENT") {
			it_local = tokens.erase(it_local);
		}
		else {
			++it_local;
		}
	}
	it = tokens.begin();
}

void GrammarParser::matchColon() {
	if (it->getType() == "COLON") {
		it++;
	}
	else {
		throw *it;
	}
}

void GrammarParser::matchSchemes() {
	if (it->getType() == "SCHEMES") {
		it++;
	}
	else {
		throw *it;
	}
}

void GrammarParser::matchScheme() {
	Scheme s = Scheme();
	if (it->getType() == "ID") {
		s.setName(*it);
		it++;
	}
	else {
		throw *it;
	}

	it->getType() == "LEFT_PAREN" ? it++ : throw *it;

	if (it->getType() == "ID") {
		s.addId(*it);
		it++;
	}
	else {
		throw *it;
	}

	s.addIds(*(matchIdList()));

	it->getType() == "RIGHT_PAREN" ? it++ : throw *it;
	dp->addScheme(s);
}

std::list<token>* GrammarParser::matchIdList() {
	std::list<token>* ids = new std::list<token>();
	while (it->getType() == "COMMA") {
		it++;
		if (it->getType() == "ID") {
			ids->push_back(*it);
			it++;
		}
		else {
			throw *it;
		}
	}
	return ids;
}

void GrammarParser::matchSchemeList() {
	while (it->getType() != "FACTS") {
		matchScheme();
	}
}

void GrammarParser::matchFacts() {
	if (it->getType() == "FACTS") {
		it++;
	}
	else {
		throw *it;
	}
}

void GrammarParser::matchFact() {
	Fact f = Fact();
	if (it->getType() == "ID") {
		f.setName(*it);
		it++;
	}
	else {
		 throw *it;
	}
	it->getType() == "LEFT_PAREN" ? it++ : throw *it;
	if (it->getType() == "STRING") {
		f.addStr(*it);
		it++;
	}
	else {
		 throw *it;
	}
	f.addStrs(*(matchStringList()));
	it->getType() == "RIGHT_PAREN" ? it++ : throw *it;
	it->getType() == "PERIOD" ? it++ : throw *it;
	dp->addFact(f);
}

std::list<token>* GrammarParser::matchStringList() {
	std::list<token>* strs = new std::list<token>();
	while (it->getType() == "COMMA") {
		it++;
		if (it->getType() == "STRING") {
			strs->push_back(*it);
			it++;
		}
		else {
			throw *it;
		}
	}
	return strs;
}

void GrammarParser::matchFactList() {
	while (it->getType() != "RULES") {
		matchFact();
	}
}

void GrammarParser::matchRules() {
	if (it->getType() == "RULES") {
		it++;
	}
	else {
		throw *it;
	}
}

void GrammarParser::matchRuleList() {
	while (it->getType() != "QUERIES") {
		matchRule();
	}
}

void GrammarParser::matchRule() {
	Rule r = Rule();
	r.setHeadPredicate(matchHeadPredicate());
	it->getType() == "COLON_DASH" ? it++ : throw *it;
	Predicate p = matchPredicate();
	r.addPredicate(p);
	std::list<Predicate>* lst = new std::list<Predicate>();
	while (it->getType() == "COMMA") {
		it++;
		lst->push_back(matchPredicate());
	}
	r.addPredicates(*lst);
	it->getType() == "PERIOD" ? it++ : throw *it;
	dp->addRule(r);
}

HeadPredicate GrammarParser::matchHeadPredicate() {
	HeadPredicate hp = HeadPredicate();
	if (it->getType() == "ID") {
		hp.setName(*it);
		it++;
	}
	else {
		throw *it;
	}
	it->getType() == "LEFT_PAREN" ? it++ : throw *it;
	if (it->getType() == "ID") {
		hp.addId(*it);
		it++;
	}
	else {
		throw *it;
	}
	hp.addIds(*(matchIdList()));
	it->getType() == "RIGHT_PAREN" ? it++ : throw *it;
	return hp;
}

std::list<Predicate>* GrammarParser::matchPredicateList() {
	std::list<Predicate>* lst = new std::list<Predicate>();
	while (it->getType() == "COMMA") {
		it++;
		lst->push_back(matchPredicate());
	}
	return lst;
}

Predicate GrammarParser::matchPredicate() {
	Predicate p;
	if (it->getType() == "ID") {
		p.setName(*it);
		it++;
	}
	else {
		throw *it;
	}
	it->getType() == "LEFT_PAREN" ? it++ : throw *it;
	p.addParam(matchParameter());
	matchParameterList(p);
	it->getType() == "RIGHT_PAREN" ? it++ : throw *it;
	return p;
}

Parameter* GrammarParser::matchParameter() {
	std::string type = it->getType();
	if (type == "STRING" || type == "ID") {
		Parameter* p = new Parameter(it->getValue());
		it++;
		return p;
	}
	else {
		Expression* e = matchExpression();
		return e;
	}
}

Expression* GrammarParser::matchExpression() {
	it->getType() == "LEFT_PAREN" ? it++ : throw *it;
	Expression* e = new Expression();
	e->setLeft(matchParameter());
	e->setOperator(matchOperator());
	e->setRight(matchParameter());
	it->getType() == "RIGHT_PAREN" ? it++ : throw *it;
	return e;
}

void GrammarParser::matchOperator(Predicate& p) {
	std::string type = it->getType();
	if (type == "ADD" || type == "MULTIPLY") {
		p.addParam(new Parameter(it->getValue()));
		it++;
	}
	else {
		throw *it;
	}
}

char GrammarParser::matchOperator() {
	std::string type = it->getType();
	if (type == "ADD" || type == "MULTIPLY") {
		char ret = (it->getValue())[0];
		it++;
		return ret;
	}
	else {
		throw *it;
	}
}

void GrammarParser::matchParameterList(Predicate& p) {
	while (it->getType() == "COMMA") {
		it++;
		p.addParam(matchParameter());
	}
}

void GrammarParser::matchQueries() {
	if (it->getType() == "QUERIES") {
		it++;
	}
	else {
		throw *it;
	}
}

void GrammarParser::matchQuery() {
	Predicate query = matchPredicate();
	it->getType() == "Q_MARK" ? it++ : throw *it;
	dp->addQuery(query);
}

void GrammarParser::matchQueryList() {
	while (it->getType() != "EOF") {
		matchQuery();
	}
}

void GrammarParser::matchEnd() {
	if (it->getType() == "EOF") {
		it++;
	}
	else {
		throw *it;
	}
}
