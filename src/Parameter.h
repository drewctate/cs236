/*
 * Parameter.h
 *
 *  Created on: Oct 6, 2016
 *      Author: drewctate
 */

#ifndef PARAMETER_H_
#define PARAMETER_H_

#include <sstream>
#include <iostream>

class Expression;

class Parameter {
	public:
		Parameter(std::string);
		virtual ~Parameter();

		virtual std::string toString();

		void setData(std::string);
		std::string getData();
	private:
		std::string data;
};

class Expression: public Parameter {
	public:
		Expression();
		void setOperator(char);
		void setLeft(Parameter*);
		void setRight(Parameter*);

		virtual std::string toString();
	private:
		char op;
		Parameter* left;
		Parameter* right;
};

#endif /* PARAMETER_H_ */
