/*
 * DatalogProgram.h
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#ifndef DATALOGPROGRAM_H_
#define DATALOGPROGRAM_H_
#include <list>
#include <set>
#include "Scheme.h"
#include "Fact.h"
#include "Rule.h"

class DatalogProgram {
public:
	DatalogProgram();
	virtual ~DatalogProgram();

	std::list<Scheme>* getSchemes();
	void addScheme(Scheme&);

	std::list<Fact>* getFacts();
	void addFact(Fact&);

	std::list<Rule>* getRules();
	void addRule(Rule);

	std::list<Predicate>* getQueries();
	void addQuery(Predicate&);

	std::string toString();

private:
	std::list<Scheme>* schemes;
	std::list<Fact>* facts;
	std::list<Rule>* rules;
	std::list<Predicate>* queries;
	std::set<std::string>* domain;
};

#endif /* DATALOGPROGRAM_H_ */
