/*
 * Relation.h
 *
 *  Created on: Oct 25, 2016
 *      Author: drewctate
 */

#ifndef RELATION_H_
#define RELATION_H_

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <utility>
#include <map>
#include <sstream>

class Relation {
public:
	Relation(std::string, std::list<std::string>&);
	virtual ~Relation();
	void clear();
	std::string getName();
	void setName(std::string);
	void setSchema(std::list<std::string>&);
	Relation select(std::map<std::string,std::string>&);
	Relation selectIds(std::vector<int>& positions);
	Relation project(std::list<std::string>&);
	Relation rename(std::map<std::string,std::string>&);
	Relation unionWith(Relation);
	Relation naturalJoin(Relation);
	int getSize();
	int getNumCols();
	std::string access(int i, int j);
	std::string getSchemaId(int position);
	bool addTuple(const std::vector<std::string>&);
	void addTuples(std::set< std::vector<std::string> >&);
	std::set< std::vector<std::string> > getTuples();
	std::string toString();

private:
	Relation selectStr(std::string, std::string);
	std::vector<std::string> mergeTuples(std::vector<std::string>, std::vector<std::string>, std::map<int,int> inCommon);
	std::string name;
	std::list<std::string> schema;
	std::set< std::vector<std::string> > tuples;
};

#endif /* RELATION_H_ */
