/*
 * Expression.cpp
 *
 *  Created on: Oct 10, 2016
 *      Author: drewctate
 */

#include "Parameter.h"

Expression::Expression() : Parameter(" "), op(NULL), left(NULL), right(NULL) {
}

void Expression::setLeft(Parameter* p) {
	left = p;
}

void Expression::setRight(Parameter* p) {
	right = p;
}

void Expression::setOperator(char c) {
	op = c;
}

std::string Expression::toString() {
	std::stringstream stream;
	stream << '(' << left->toString() <<
			op << right->toString() << ')';
	return stream.str();
}
