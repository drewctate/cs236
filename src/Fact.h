/*
 * Fact.h
 *
 *  Created on: Oct 4, 2016
 *      Author: drewctate
 */

#ifndef FACT_H_
#define FACT_H_

#include <list>
#include <sstream>
#include "token.h"

class Fact {
public:
	Fact();
	virtual ~Fact();

	token getName();
	void setName(token);

	std::list<token>* getStrs();
	void addStr(token);
	void addStrs(std::list<token>&);

	std::string toString();

private:
	token name;
	std::list<token>* strList;
	int strCount;
};

#endif /* FACT_H_ */
